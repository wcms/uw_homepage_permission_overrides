<?php

/**
 * @file
 * homepage_permission_overrides.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function homepage_permission_overrides_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: user_permission.
  $overrides["user_permission.access ckeditor link.roles|emergency alerter"] = 'emergency alerter';

  // Exported overrides for: variable.
  $overrides["variable.roleassign_roles.value|10"] = 0;
  $overrides["variable.roleassign_roles.value|11"] = 0; /* Was 11 */
  $overrides["variable.roleassign_roles.value|12"] = 0;
  $overrides["variable.roleassign_roles.value|13"] = 0;
  $overrides["variable.roleassign_roles.value|14"] = 0;
  $overrides["variable.roleassign_roles.value|15"] = 0;
  $overrides["variable.roleassign_roles.value|16"] = 0;
  $overrides["variable.roleassign_roles.value|17"] = 0;

  return $overrides;
}
