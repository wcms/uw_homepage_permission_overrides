<?php

/**
 * @file
 * homepage_permission_overrides.features.inc
 */

/**
 * Implements hook_user_default_permissions_alter().
 */
function homepage_permission_overrides_user_default_permissions_alter(&$data) {
  if (isset($data['access ckeditor link'])) {
    $data['access ckeditor link']['roles']['emergency alerter'] = 'emergency alerter'; /* WAS: '' */
  }
  if (isset($data['use text format uw_tf_standard_sidebar'])) {
    unset($data['use text format uw_tf_standard_sidebar']['roles']['emergency alerter']);
  }
}

/**
 * Implements hook_strongarm_alter().
 */
function homepage_permission_overrides_strongarm_alter(&$data) {
  if (isset($data['roleassign_roles'])) {
    $data['roleassign_roles']->value['10'] = 0; /* WAS: '' */
    $data['roleassign_roles']->value['11'] = 11; /* WAS: '' */
    $data['roleassign_roles']->value['12'] = 0; /* WAS: '' */
    $data['roleassign_roles']->value['13'] = 0; /* WAS: '' */
    $data['roleassign_roles']->value['14'] = 0; /* WAS: '' */
    $data['roleassign_roles']->value['15'] = 0; /* WAS: '' */
    $data['roleassign_roles']->value['16'] = 0; /* WAS: '' */
    $data['roleassign_roles']->value['17'] = 0; /* WAS: '' */
  }
}
